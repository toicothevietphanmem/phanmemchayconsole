﻿using PhanMemDauTien.Bai02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhanMemDauTien.Bai04
{
    class ChiTietDonHang
    {
        public SanPham SanPham { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien => SoLuong * DonGia;
    }
}

﻿using PhanMemDauTien.Bai02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhanMemDauTien.Bai04
{
    class PhieuBanHang
    {
        public string MaPhieu { get; set; }
        public DateTime Ngay { get; set; }
        public KhachHang KhachHang { get; set; }
        public double TongTien => ThongTinHangBan.Sum(s => s.ThanhTien);
        public double PhanTramGiam { get; set; }
        public double TienPhaiTra { get; set; }

        public List<ChiTietDonHang> ThongTinHangBan { get; set; } = new List<ChiTietDonHang>();
        public void ThemHang(SanPham sanPham, double soLuong)
        {
            var chiTietDonHang = ThongTinHangBan
                                    .Where(ct => ct.SanPham == sanPham)
                                    .FirstOrDefault();
            if (chiTietDonHang != null)
            {
                chiTietDonHang.SoLuong += soLuong;
                return;
            }

            chiTietDonHang = new ChiTietDonHang()
            {
                SanPham = sanPham,
                SoLuong = soLuong,
                DonGia=sanPham.GiaBan
            };
            ThongTinHangBan.Add(chiTietDonHang);
        }




        public void InPhieu()
        {
            Console.WriteLine("PHIEU BAN HANG");
            Console.WriteLine($"Ngay: {Ngay}");
            Console.WriteLine($"Khach Hang: {KhachHang.Ten}");
            var i = 0;
            foreach(var chiTiet in ThongTinHangBan)
            {
                i++;
                Console.WriteLine($"{i}.\t{chiTiet.SanPham.Ten20()}\t {chiTiet.SoLuong:N0}\t{chiTiet.DonGia:N0} {chiTiet.ThanhTien:N0}");
            }
            Console.WriteLine("---------------------------------------------------------------------");
            Console.WriteLine($"TONG TIEN: {TongTien:N0}");
        }
    }
}

﻿using PhanMemDauTien.Bai02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhanMemDauTien.Bai03
{
    class QuanLyKho
    {
        public List<TonKho> BangTonKho { get; set; } = new List<TonKho>();
        //Behaviour
        //Method, function
        public void NhapKho(SanPham sanPham, double soLuong)
        {
            var tonKho = BangTonKho
                    .Where(tk => tk.SanPham == sanPham)
                    .FirstOrDefault();
            if (tonKho != null)
            {
                tonKho.SoLuongTonKho += soLuong;
                return;
            }
            tonKho = new TonKho
            {
                SanPham = sanPham,
                SoLuongTonKho = soLuong
            };
            BangTonKho.Add(tonKho);
        }
        public void XuatKho(SanPham sanPham, double soLuong)
        {
            var tonKho = BangTonKho
                   .Where(tk => tk.SanPham == sanPham)
                   .FirstOrDefault();
            if (tonKho != null)
            {
                tonKho.SoLuongTonKho -= soLuong;
                return;
            }
            tonKho = new TonKho
            {
                SanPham = sanPham,
                SoLuongTonKho = 0-soLuong
            };
            BangTonKho.Add(tonKho);
        }

        public void BaoCaoTonKho()
        {
            //statement
            Console.WriteLine("====================================");
            Console.WriteLine("BÁO CÁO TỒN KHO");
            foreach(var tonKho in BangTonKho)
            {
                Console.WriteLine($"{tonKho.SanPham.Ten}: {tonKho.SoLuongTonKho:N2}");
            }
        }

    }
}

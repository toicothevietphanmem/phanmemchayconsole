﻿using PhanMemDauTien.Bai02;
using PhanMemDauTien.Bai03;
using PhanMemDauTien.Bai04;
using System;

namespace PhanMemDauTien
{
    class Program
    {
        static void Main(string[] args)
        {
            var heineiken = new SanPham
            {
                Ma = "SP-0001",
                Ten = "Heineiken",
                DonViTinh = "Lon",
                GiaMua = 18000,
                GiaBan = 20000
            };
            var tiger = new SanPham
            {
                Ma = "SP-0002",
                Ten = "Tiger",
                DonViTinh = "Lon",
                GiaMua = 16000,
                GiaBan = 18000
            };
            var saporo = new SanPham
            {
                Ma = "SP-0003",
                Ten = "Saporo",
                DonViTinh = "Lon",
                GiaMua = 22000,
                GiaBan = 26000
            };

            var nhien = new KhachHang();
            nhien.Ma = "KH-001";
            nhien.Ten = "Sử Nhiên";
            nhien.DiaChi = "Bình Thạnh";
            var donHang = new PhieuBanHang()
            {
                Ngay=DateTime.Now,
                KhachHang=nhien,
            };
            donHang.ThemHang(heineiken, 10);
            donHang.ThemHang(tiger, 5);
            donHang.ThemHang(saporo, 6);
            donHang.ThemHang(heineiken, 14);
            donHang.InPhieu();
            Console.ReadLine();
        }
    }
}

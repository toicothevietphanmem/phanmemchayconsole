﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhanMemDauTien.Bai02
{
    class SanPham
    {
        //Encapsulation: 
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string DonViTinh { get; set; }

        public double GiaMua { get; set; }
        public double GiaBan { get; set; }

        public override string ToString()
        {
            return $"{Ma}/{Ten}/{DonViTinh}. Giá mua: {GiaMua:N2}d, Gia Ban: {GiaBan:N2}d";
        }

        public string Ten20()
        {
           
            return Ten.PadRight(20, ' ');
        }
    }
}
